FROM php:7.4-apache

# Enable Apache modules
RUN a2enmod rewrite
# Install any extensions you need
RUN apt-get update && apt-get install -y \
        libzip-dev \
        zip \
  && docker-php-ext-install zip
RUN docker-php-ext-configure zip

RUN docker-php-ext-install mysqli pdo pdo_mysql

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

WORKDIR /var/www/html
COPY 000-default.conf /etc/apache2/sites-available/000-default.conf

COPY ./ .